package org.databandtech.flink.demo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.elasticsearch.ElasticsearchSinkFunction;
import org.apache.flink.streaming.connectors.elasticsearch.RequestIndexer;
import org.apache.flink.streaming.connectors.elasticsearch7.ElasticsearchSink;
import org.apache.http.HttpHost;
import org.databandtech.flink.source.SourceFromMySQLByTuple5;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.Requests;

/**
 * ElasticSearch Sink
 * @author Administrator
 *
 */
public class SourceByJDBCSinkToElasticSearch {
	
	static int BULK_FLUSH_MAX_ACTIONS = 100; //每一个批次处理的数量，否则就缓存后Flush
	static int STREAM_SINK_PARALLELISM = 4;//并行线程
	//static String INDEX = "user";
	static String INDEX = "databand_video";
	//static String[] columnsRead = new String[] {"name","age"};//定義字段名
	static String[] columnsRead = new String[] {"title","status","vid","type_name","url"};//定義字段名
	
	public static void main(String[] args) {
		
		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		
		env.setRestartStrategy(RestartStrategies.fixedDelayRestart(3,3000));
        String url = "jdbc:mysql://XXX.53.132.XXX:7360/launcher_media?useUnicode=true&characterEncoding=utf-8&useSSL=false";
        String user = "root";
        String pass = "mysql#";
        //String sqlRead = "select name,age from user";
        String sqlRead = "SELECT title,status,vid,type_name,url from databand_video ORDER BY id DESC LIMIT 100000";
        

        //######### 数据读取 #########
        //取2个字段
        //DataStreamSource<Tuple2<String,Integer>> userDataStreamSource = env.addSource(
        //new SourceFromMySQL(url,user,pass,columnsRead,sqlRead));

        //取5个字段,10万条数据灌入大概5秒
        DataStreamSource<Tuple5<String,Integer,String,String,String>> userDataStreamSource = env.addSource(
				new SourceFromMySQLByTuple5(url,user,pass,columnsRead,sqlRead));
		userDataStreamSource.print();
       
        List<HttpHost> httpHosts = new ArrayList<>();
        httpHosts.add(new HttpHost("127.0.0.1", 9200, "http"));
		//######### ES写入  #########
		//第一种方式，不用转换的直接灌数据
		addSinkByDataStreamSourceByTuple5(httpHosts,BULK_FLUSH_MAX_ACTIONS,userDataStreamSource,INDEX);
		//第二种方式，需要转换后灌数据
		//map 規則是30歲以下的獎金 1000，30-60嵗獎金2000，其餘的3000
		//SingleOutputStreamOperator<Tuple3<String,Integer,BigDecimal>> dataWriteStream = userDataStreamSource.map(new MapTransformation());
		//dataWriteStream.print();
		//ESSinkUtil.addSink(httpHosts, BULK_FLUSH_MAX_ACTIONS, sinkParallelism, dataSingleOutputStream,new ESSinkFunction("a"));
		
		try {
			env.execute("ok-es");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void addSinkByDataStreamSourceByTuple5(List<HttpHost> httpHosts, int bulkFlushMaxActions,
			DataStreamSource<Tuple5<String, Integer, String, String, String>> userDataStreamSource, String index) {

		ElasticsearchSink.Builder<Tuple5<String, Integer, String, String, String>> esSinkBuilder = new ElasticsearchSink.Builder<>(
			    httpHosts,
			    new ElasticsearchSinkFunction<Tuple5<String, Integer, String, String, String>>() {

					private static final long serialVersionUID = 1L;

					public IndexRequest createIndexRequest(Tuple5<String, Integer, String, String, String> element) {
			            Map<String, Object> json = new HashMap<>();
			            int index = 0;
			            for (String column : columnsRead) {
			            	json.put(column, element.getField(index));
			            	index++;
			            }
			            return Requests.indexRequest()
			                    .index(INDEX)
			                    .source(json);
			        }

			        @Override
			        public void process(Tuple5<String, Integer, String, String, String> element, RuntimeContext ctx, RequestIndexer indexer) {
			            indexer.add(createIndexRequest(element));
			        }
			    }
			);
		esSinkBuilder.setBulkFlushMaxActions(bulkFlushMaxActions);
		userDataStreamSource.addSink(esSinkBuilder.build());
	}
	
	public static class MapTransformation 
			implements MapFunction<Tuple2<String,Integer>,Tuple3<String,Integer,BigDecimal>> {

		private static final long serialVersionUID = 1L;

		@Override
		public Tuple3<String, Integer, BigDecimal> map(Tuple2<String, Integer> in) throws Exception {
			Tuple3<String, Integer, BigDecimal> result = new Tuple3<String, Integer, BigDecimal>();
			result.setField(in.f0, 0);
			result.setField(in.f1, 1);
			
			BigDecimal money = BigDecimal.valueOf(0);
			if (in.f1< 30)
				money = BigDecimal.valueOf(1000);
			else if (in.f1>= 30 && in.f1< 60)
				money = BigDecimal.valueOf(2000);
			else
				money = BigDecimal.valueOf(3000);
				
			result.setField(money, 2);
			
			return null;
		}
	}
}
