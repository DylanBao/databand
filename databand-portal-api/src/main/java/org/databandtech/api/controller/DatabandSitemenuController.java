package org.databandtech.api.controller;

import java.util.List;

import org.databandtech.api.entity.DatabandSitemenu;
import org.databandtech.api.entity.SysMenu;
import org.databandtech.api.entity.core.AjaxResult;
import org.databandtech.api.service.IDatabandSitemenuService;
import org.databandtech.api.service.ISysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 站点菜单Controller
 * 
 * @author databand
 * @date 2020-12-31
 */
@Controller
@RequestMapping("/api/sitemenu")
public class DatabandSitemenuController {
	protected String prefix = "/api/sitemenu";

	@Autowired
	private IDatabandSitemenuService databandSitemenuService;

	@Autowired
	private ISysMenuService menuService;

	@GetMapping("/list")
	@ResponseBody
	public List<DatabandSitemenu> list(DatabandSitemenu databandSitemenu) {
		List<DatabandSitemenu> list = databandSitemenuService.selectDatabandSitemenuList(databandSitemenu);
		return list;
	}

	@GetMapping("/listBySiteId/{siteid}")
	@ResponseBody
	public List<DatabandSitemenu> listBySiteId(@PathVariable("siteid") Long siteid) {

		List<DatabandSitemenu> list = databandSitemenuService.selectDatabandSitemenuListBySiteid(siteid);
		return list;
	}

	@GetMapping("/getRouters/{siteid}")
	@ResponseBody
	public AjaxResult getRouters(@PathVariable("siteid") Long siteid) {
		List<SysMenu> menus = menuService.selectMenuTreeBySiteId(siteid);
		return AjaxResult.success(menuService.buildMenus(menus));
	}
	
	@GetMapping("/getRouters1/{siteid}")
	@ResponseBody
	public List<SysMenu> getRouters1(@PathVariable("siteid") Long siteid) {
		List<SysMenu> menus = menuService.selectMenuTreeBySiteId(siteid);
		return menus;
	}


}
