package com.ruoyi.web.mapper;

import java.util.List;
import com.ruoyi.web.domain.DatabandReporttab;

/**
 * 报页Mapper接口
 * 
 * @author databand
 * @date 2020-12-31
 */
public interface DatabandReporttabMapper 
{
    /**
     * 查询报页
     * 
     * @param id 报页ID
     * @return 报页
     */
    public DatabandReporttab selectDatabandReporttabById(Long id);

    /**
     * 查询报页列表
     * 
     * @param databandReporttab 报页
     * @return 报页集合
     */
    public List<DatabandReporttab> selectDatabandReporttabList(DatabandReporttab databandReporttab);

    /**
     * 新增报页
     * 
     * @param databandReporttab 报页
     * @return 结果
     */
    public int insertDatabandReporttab(DatabandReporttab databandReporttab);

    /**
     * 修改报页
     * 
     * @param databandReporttab 报页
     * @return 结果
     */
    public int updateDatabandReporttab(DatabandReporttab databandReporttab);

    /**
     * 删除报页
     * 
     * @param id 报页ID
     * @return 结果
     */
    public int deleteDatabandReporttabById(Long id);

    /**
     * 批量删除报页
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDatabandReporttabByIds(String[] ids);
}
