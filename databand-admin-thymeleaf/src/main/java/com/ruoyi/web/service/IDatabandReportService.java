package com.ruoyi.web.service;

import java.util.List;
import com.ruoyi.web.domain.DatabandReport;

/**
 * 报Service接口
 * 
 * @author databand
 * @date 2020-12-31
 */
public interface IDatabandReportService 
{
    /**
     * 查询报
     * 
     * @param id 报ID
     * @return 报
     */
    public DatabandReport selectDatabandReportById(Long id);

    /**
     * 查询报列表
     * 
     * @param databandReport 报
     * @return 报集合
     */
    public List<DatabandReport> selectDatabandReportList(DatabandReport databandReport);
    public List<DatabandReport> selectDatabandReportList();

    /**
     * 新增报
     * 
     * @param databandReport 报
     * @return 结果
     */
    public int insertDatabandReport(DatabandReport databandReport);

    /**
     * 修改报
     * 
     * @param databandReport 报
     * @return 结果
     */
    public int updateDatabandReport(DatabandReport databandReport);

    /**
     * 批量删除报
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteDatabandReportByIds(String ids);

    /**
     * 删除报信息
     * 
     * @param id 报ID
     * @return 结果
     */
    public int deleteDatabandReportById(Long id);
}
