package com.ruoyi.web.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.web.mapper.DatabandScheduletaskMapper;
import com.ruoyi.web.domain.DatabandScheduletask;
import com.ruoyi.web.service.IDatabandScheduletaskService;
import com.ruoyi.common.core.text.Convert;

/**
 * 批处理计划Service业务层处理
 * 
 * @author databand
 * @date 2020-12-31
 */
@Service
public class DatabandScheduletaskServiceImpl implements IDatabandScheduletaskService 
{
    @Autowired
    private DatabandScheduletaskMapper databandScheduletaskMapper;

    /**
     * 查询批处理计划
     * 
     * @param id 批处理计划ID
     * @return 批处理计划
     */
    @Override
    public DatabandScheduletask selectDatabandScheduletaskById(Long id)
    {
        return databandScheduletaskMapper.selectDatabandScheduletaskById(id);
    }

    /**
     * 查询批处理计划列表
     * 
     * @param databandScheduletask 批处理计划
     * @return 批处理计划
     */
    @Override
    public List<DatabandScheduletask> selectDatabandScheduletaskList(DatabandScheduletask databandScheduletask)
    {
        return databandScheduletaskMapper.selectDatabandScheduletaskList(databandScheduletask);
    }

    /**
     * 新增批处理计划
     * 
     * @param databandScheduletask 批处理计划
     * @return 结果
     */
    @Override
    public int insertDatabandScheduletask(DatabandScheduletask databandScheduletask)
    {
        databandScheduletask.setCreateTime(DateUtils.getNowDate());
        return databandScheduletaskMapper.insertDatabandScheduletask(databandScheduletask);
    }

    /**
     * 修改批处理计划
     * 
     * @param databandScheduletask 批处理计划
     * @return 结果
     */
    @Override
    public int updateDatabandScheduletask(DatabandScheduletask databandScheduletask)
    {
        databandScheduletask.setUpdateTime(DateUtils.getNowDate());
        return databandScheduletaskMapper.updateDatabandScheduletask(databandScheduletask);
    }

    /**
     * 删除批处理计划对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteDatabandScheduletaskByIds(String ids)
    {
        return databandScheduletaskMapper.deleteDatabandScheduletaskByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除批处理计划信息
     * 
     * @param id 批处理计划ID
     * @return 结果
     */
    @Override
    public int deleteDatabandScheduletaskById(Long id)
    {
        return databandScheduletaskMapper.deleteDatabandScheduletaskById(id);
    }
}
