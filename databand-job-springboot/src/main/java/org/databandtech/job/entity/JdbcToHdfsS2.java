package org.databandtech.job.entity;

/**
 * Sqoop2版的实体类
 *
 */
public class JdbcToHdfsS2 {

	boolean isFull;	
	String fromLink;
	String toLink;
	String jobName;
	String fromschemaName;
	String fromTable;
	String partitionColumn;
	String hdfsOutputDirectory;
	String numExtractors;

	public String getFromLink() {
		return fromLink;
	}
	public void setFromLink(String fromLink) {
		this.fromLink = fromLink;
	}
	public String getToLink() {
		return toLink;
	}
	public void setToLink(String toLink) {
		this.toLink = toLink;
	}
	public boolean isFull() {
		return isFull;
	}
	public void setFull(boolean isFull) {
		this.isFull = isFull;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getFromschemaName() {
		return fromschemaName;
	}
	public void setFromschemaName(String fromschemaName) {
		this.fromschemaName = fromschemaName;
	}
	public String getFromTable() {
		return fromTable;
	}
	public void setFromTable(String fromTable) {
		this.fromTable = fromTable;
	}
	public String getPartitionColumn() {
		return partitionColumn;
	}
	public void setPartitionColumn(String partitionColumn) {
		this.partitionColumn = partitionColumn;
	}
	public String getHdfsOutputDirectory() {
		return hdfsOutputDirectory;
	}
	public void setHdfsOutputDirectory(String hdfsOutputDirectory) {
		this.hdfsOutputDirectory = hdfsOutputDirectory;
	}
	public String getNumExtractors() {
		return numExtractors;
	}
	public void setNumExtractors(String numExtractors) {
		this.numExtractors = numExtractors;
	}
}
