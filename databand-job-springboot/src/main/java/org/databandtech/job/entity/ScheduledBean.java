package org.databandtech.job.entity;

public class ScheduledBean {
	
	private String jobtype;
	private String jobcode;
	private String beaname;
	private String methodname;
	private String methodparams;
	private String descri;
	private String cron;
	private String ext;	
	//-1:未执行；0:执行失败；1：执行成功
    private boolean status;
	//0:未启动；1：已启动
    private boolean startflag;

    public String getJobtype() {
		return jobtype;
	}
	public void setJobtype(String jobtype) {
		this.jobtype = jobtype;
	}
	public String getJobcode() {
		return jobcode;
	}
	public void setJobcode(String jobcode) {
		this.jobcode = jobcode;
	}
	public String getBeaname() {
		return beaname;
	}
	public void setBeaname(String beaname) {
		this.beaname = beaname;
	}
	public String getMethodname() {
		return methodname;
	}
	public void setMethodname(String methodname) {
		this.methodname = methodname;
	}
	public String getMethodparams() {
		return methodparams;
	}
	public void setMethodparams(String methodparams) {
		this.methodparams = methodparams;
	}
	public String getdescri() {
		return descri;
	}
	public void setdescri(String descri) {
		this.descri = descri;
	}
	public String getCron() {
		return cron;
	}
	public void setCron(String cron) {
		this.cron = cron;
	}
	public String getExt() {
		return ext;
	}
	public void setExt(String ext) {
		this.ext = ext;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public boolean isStartflag() {
		return startflag;
	}
	public void setStartflag(boolean startflag) {
		this.startflag = startflag;
	}
}
