package org.databandtech.redis;

import java.util.UUID;
import org.redisson.Redisson;
import org.redisson.api.RBloomFilter;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

/**
 * Hello world!
 *
 */
public class App 
{
	static int MAXCOUNT = 10000;
    public static void main( String[] args )
    {       
        //redisBloomInit();//第一次执行一次，用于创建bloom项目
        //redisBloomMatch();
    	
    	redisBloomInitString();
    	//redisBloomMatchStr();
    }

	private static void redisBloomInitString() {
        System.out.println( "第一次执行一次，用于创建bloom项目" );
		Config config = new Config();
		config.useSingleServer().setAddress("redis://192.168.13.215:6379");
		RedissonClient redisson = Redisson.create(config);
		
		long containerCount = 10000000L;
		
		RBloomFilter<String> bloomFilter = redisson.getBloomFilter("useridStrList2");
		bloomFilter.tryInit(containerCount,0.1);

		for (int i=0;i<MAXCOUNT;i++) {
			String uuid = UUID.randomUUID().toString().replaceAll("-","");
			bloomFilter.add(uuid);
		}
		System.out.println( "创建bloom项目结束" );
	}
    
	private static void redisBloomMatchStr() {
        System.out.println( "测试 redis的布隆过滤器的碰撞次数" );
		Config config = new Config();
		config.useSingleServer().setAddress("redis://192.168.13.118:6379");
		RedissonClient redisson = Redisson.create(config);
		
		int errorTimes = 0;
		int start = MAXCOUNT +1;
		int end = MAXCOUNT +10000000;
		
		RBloomFilter<String> bloomFilter = redisson.getBloomFilter("useridStrList2");

		System.out.println("总计："+bloomFilter.count());
		String uuid = UUID.randomUUID().toString().replaceAll("-","");
		long starttime = System.currentTimeMillis();
		bloomFilter.contains(uuid);
		long endtime = System.currentTimeMillis();
		long duration = endtime - starttime;
		System.out.println("单项的匹配时长： "+duration);

		
		//判断在布隆过滤器中的碰撞次数
		for (int i=start;i<end;i++) {
			String uuid1 = UUID.randomUUID().toString().replaceAll("-","");
			if (bloomFilter.contains(uuid1)) {
				errorTimes++;
			}
		}
        System.out.println( "误判次数："+errorTimes );
	}
	
    
}
