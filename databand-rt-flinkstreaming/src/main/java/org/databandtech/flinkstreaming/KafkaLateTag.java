package org.databandtech.flinkstreaming;

import org.apache.flink.util.OutputTag;
import org.databandtech.flinkstreaming.Entity.EpgVod;

public class KafkaLateTag extends OutputTag<EpgVod> {

	private static final long serialVersionUID = 7312785190153211388L;

	public KafkaLateTag(String id) {
		super(id);
		// 处理延迟输出
	}

}
